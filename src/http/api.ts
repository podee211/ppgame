import httpRequest from './http'

export const payTypeList = (data: any) => httpRequest(
    'post',
    `/tel-api/deposit/payTypeList`,
    data
)

export const depositOrder = (data: any) => httpRequest(
    'post',
    `/tel-api/deposit/depositOrder`,
    data
)

export const getUserInfo = () => httpRequest(
    'get',
    `/tel-api/telApi/getUserMoney`
)
export const getGame = (data: any) => httpRequest(
    'get',
    `/tel-api/telApi/getToken/${data.gameId}/${data.vendorId}`
)
export const getDiamondsByTime = (data: any) => httpRequest(
    'post',
    `/tel-api/telApi/getDiamondsByTime`,
    data
)


export const clickDiamond = (data: any) => httpRequest(
    'post',
    `/tel-api//telApi/clickDiamond`,
    data
)

  