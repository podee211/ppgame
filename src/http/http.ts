import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import configs from "@/utils/config";
import { Toast } from "vant";
import store from '@/store/index';
import Router from "@/router";

// Helper function to generate base request configuration
const getBaseRequestConfig = (data: any = {}) => ({
  baseURL: '/tel-api',
  appVer: configs.appVer,
  ver: configs.protocolVer,
  reqId: configs.reqId,
  param: data,
});

const instance = axios.create({
  timeout: 30000,
});

instance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
let loginRequest = 0;

// 登录过期处理
const loginInvalid = () => {
  setTimeout(() => {
    Router.push({
      name: "home",
    });
  }, 500);
};

instance.interceptors.request.use((config: any) => {
  config.headers = {
    "X-Frame-Options": "SAMEORIGIN",
    contentType: "application/json",
    clientCode: (window as any).clientCode,
    clientItem: (window as any).childCode,
    skinCode: (window as any).theme,
    appId: (window as any).theme,
    'accept-language': `en-US,en;q=0.9`
  };
  const { userInfo } = store.getters;
  if (userInfo?.apitoken) {
    config.headers["apitoken"] = userInfo?.apitoken;
  } else {
    config.headers["apitoken"] = "";
  }
  return config;
});

instance.interceptors.response.use(
  (response: any) => {
    switch (response.data.code) {
      case 16003: // 其他设备登录
        if (loginRequest === 0) {
          loginRequest = 1;
          Toast.fail(response.data.msg || '您已在其他设备登录');
          loginInvalid();
        }
        break;
      case 110009: // IP限制
        Router.push({
          name: "iplimit",
          query: {
            ip: response.data.msg,
            customerUrl: response.data.data?.customer, // Use optional chaining
          },
        });
        break;
      default:
        if (response.status === 401) return Promise.reject(response); // 抛出错误
        return response;
    }
  },
  (error: any) => {
    switch (error.response.status) {
      case 503:
      case 500:
        Toast.error("服务器报错");
        break;
      case 401:
        if (loginRequest === 0) {
          loginRequest = 1;
          Toast.fail("您的登录账户已经过期，请您重新登录");
          loginInvalid();
        }
        break;
      default:
        if (error.response.data && error.response.data.msg) {
          Toast.fail(error.response.data.msg);
        }
        break;
    }
    if (loginRequest === 1 && error.response.status === 401) return Promise.reject(error); // 抛出错误
    return Promise.resolve(error.response);
  }
);

// Function to handle HTTP requests
const httpRequest = <T>(method: "post" | "get" | "put" | "delete", url: string, data?: any): Promise<T> => {
  const requestUrl = configs.host + url;
  const baseConfig = getBaseRequestConfig(data);
  
  // Axios request based on method
  switch (method) {
    case "post":
      return instance.post(requestUrl, baseConfig.param);
    case "get":
      return instance.get(requestUrl);
    case "put":
      return instance.put(requestUrl, baseConfig.param);
    default:
      throw new Error(`Unsupported method: ${method}`);
  }
};

// Specific HTTP methods
export const _post = <T>(url: string, data?: any): Promise<T> => httpRequest<T>("post", url, data);
export const _get = <T>(url: string, params?: string): Promise<T> => httpRequest<T>("get", url, params);
// export const _get = <T>(url: string, params?: string): Promise<T> => axios.get(configs.host + url + params);


export default httpRequest; // Export the general httpRequest function for more flexibility
