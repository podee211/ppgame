import { getUserInfo } from '@/http/api';
import { createStore } from 'vuex'

export default createStore({
  state: {
    userInfo: {},
  },
  getters: {
    userInfo: state => state.userInfo,
  },
  mutations: {
    SET_USER_INFO(state, data) {
      state.userInfo = data;
      console.log(data);
      
      localStorage.setItem('store', JSON.stringify(data));
    },
  },
  actions: {
    SET_USER_INFO({ commit }, data) {
      commit('SET_USER_INFO', data);
    },
    async initializeStore({ commit }) {
      const query = new URLSearchParams(window.location.search);
      let searchs: any = {}
      if (query.size) {
        searchs = {
          apitoken: query.get('apitoken'),
          username: query.get('username'),
          gameId: query.get('gameId'),
          vendorId: query.get('vendorId')
        }
        commit('SET_USER_INFO', searchs);
        const res: any = await getUserInfo()
        if (res.data.code === 200) {
          commit('SET_USER_INFO', {...searchs, ...res.data.data});
        }
      } else if (localStorage.getItem('store')) {
        commit('SET_USER_INFO', JSON.parse(localStorage.getItem('store') || ''));
      }
    }
  },
  modules: {
  }
})
