import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/home',
    name: 'homePage',
    component: () => import('../views/home.vue'),
    children: [
      {
        path: '/record',
        name: 'recordList',
        component: () => import('../views/record.vue')
      },
      {
        path: '/addup',
        name: 'addupPage',
        component: () => import('../views/addup.vue')
      }
    ]
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/home'
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
