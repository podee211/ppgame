import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "@/style/index.scss"
import { Calendar, Pagination, Toast } from 'vant';
import { Locale } from 'vant';
import i18n, {_lang} from './locale'
// 引入英文语言包
import enES from 'vant/es/locale/lang/es-ES';
Locale.use('es-ES', enES);
import 'vant/lib/index.css';

const app = createApp(App)
store.dispatch('initializeStore'); 
app.use(store).use(i18n).use(router).use(Toast).use(Calendar).use(Pagination).mount('#app')
