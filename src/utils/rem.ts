
window.onresize = function () {
    setRem()
}
function setRem() {
    const oWidth = document.body.clientWidth || document.documentElement.clientWidth
    if (oWidth > 768) {
        getRem(1920, 100)
    } else {
        getRem(375, 100)
    }
}
function getRem(pwidth: number, prem: number) {
    const html = document.getElementsByTagName("html")[0]
    const oWidth = document.body.clientWidth || document.documentElement.clientWidth
    html.style.fontSize = oWidth / pwidth * prem + "px"
}