export default {
    host: "", // 新测试环境
    loginVersion: "v1.0.0",
    baseUrl: "",
    dowUrl: "https://appdownload.011312.com/",
    customerServiceUrl: "",
    // 页面服务器
    codeUrl: "",
    // 图片服务器
    imgHost: "https://web.bets8888.life/file",
    appVer: "1.0.0",
    protocolVer: "1.0",
    reqId: 0,
    numVer: "v2.0.1",
    themeIndex: 1,
    clientIp: "101.231.217.194",
    locale: sessionStorage.getItem("locale"), // 国际化  zh_CN中文健体   zh_TW中文繁体  en英文
  };
  