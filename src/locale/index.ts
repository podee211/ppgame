import { createI18n } from 'vue-i18n'
import crc32 from 'crc/crc32'
import zh from './json/zh.json'
import en from './json/en.json'

const defaultLang = 'zh'
const i18n = createI18n({
  legacy: false, // 是否允许在 Legacy API 模式下使用 Composition API
  locale: defaultLang,
  fallbackLocale: defaultLang,
  globalInjection: true,
  silentTranslationWarn: true, // 去除国际化警告
  messages: {
    zh,
    en
  }
})

// --------这里是i18next-scanner新增的配置-------------
function lang(key: any, params: any) {
  const hashKey = `K${crc32(key).toString(16)}` // 将中文转换成crc32格式去匹配对应的json语言包
  let words = i18n.global.t(hashKey)
  if (words === hashKey) {
    words = key
  }

  // 配置传递参数的场景, 目前仅支持数组，可在此拓展
  if (Array.isArray(params)) {
    const reg = /\((\d)\)/g
    words = words.replace(reg, (a, b) => {
      return params[b]
    })
  }
  return words
}
// --------这里是i18next-scanner新增的配置-------------
window.$t = lang
export const _lang = lang
export default i18n