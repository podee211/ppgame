(function () {
	function t() {
		var c = [
			{
				"code": "ff10",
				"skin": "bgame",
				"childCode": "ff10",
				"locale": 'zh_CN',
				"name": "bgame",
				"theme": "a134",
				"winOpenTime": "6000",
				"customerServiceStatus": 1, //0旧版本   1新版本
				"sortArr": [1, 2, 3, 4, 5, 6, 7],
				"maintainUrl": "",
				"domains": [],
				"currencySymbol": '$'
			}
		]
		var o = c[0];
		o.host = "https://tc20019.com/xxa" // 线上
		localStorage.setItem("host", o.host);
		localStorage.setItem("domainName", o.domainName);
		localStorage.setItem("statistics", o.statistics);
		sessionStorage.setItem("theme", parseInt(o.theme.slice(1)));
		window.theme = o.theme;
		window.clientCode = o.code;
		window.projectImgUrl = o.skin;
		window.projectName = o.name;
		window.winOpenTime = o.winOpenTime;
		window.childCode = o.childCode;
		window.mergeAccount = o.mergeAccount;
		window.maintainUrl = o.maintainUrl;
		window.locale = o.locale;
		window.interval = 180000;
		document.title = o.name;
		window.sortArr = o.sortArr;
		window.customerServiceStatus = o.customerServiceStatus;
		window.currencySymbol = o.currencySymbol
	};
	t();
})();