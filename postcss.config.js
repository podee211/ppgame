module.exports = {
    plugins: {
      'postcss-px-to-viewport': {
        viewportWidth: 375, // 视口宽度，对应设计稿宽度
        viewportHeight: 667, // 视口高度，对应设计稿高度，可以不配置
        unitPrecision: 3, // 转换后的小数位数
        viewportUnit: 'vw', // 转换后的单位
        selectorBlackList: [], // 不进行转换的类名
        minPixelValue: 1, // 小于或等于1px的不转换
        mediaQuery: false, // 允许在媒体查询中转换px
      },
    },
  };
  