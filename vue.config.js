const { defineConfig } = require('@vue/cli-service')
// const { VantResolver } = require('@vant/auto-import-resolver');
// const AutoImport = require('unplugin-auto-import/webpack');
// const Components = require('unplugin-vue-components/webpack');
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/tel-api': {
        target: 'https://dice-admin.fdxbm8wqqcyswr4q.com/',
        changeOrigin: true,
      }
    },
  },
})
